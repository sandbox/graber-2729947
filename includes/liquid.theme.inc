<?php
/**
 * @file
 * Module theme functions.
 */

use Drupal\Core\Template\Attribute;

/**
 * Convert variables and move theme implementation to main liquid_slider theme.
 */
function theme_liquid_slider_view($variables) {
  $view = $variables['view'];
  $rows = $variables['rows'];

  $output = array(
    '#theme' => 'liquid_slider',
    '#slides' => [],
    '#slider_id' => 'slider-' . $view->id() . '-' . $view->current_display
  );

  $style = $view->style_plugin;

  // View's options.
  $options = $style->options;
  $output['#settings']['json'] = $options['json'];

  foreach ($rows as $id => $row) {
    $output['#slides'][$id] = $row;
    $output['#slides'][$id]['content'] = $row;
    $attributes = array();
    if ($row_class = $view->style_plugin->getRowClass($id)) {
      $attributes['class'][] = $row_class;
    }
    $output['#slides'][$id]['attributes'] = new Attribute($attributes);
  }

  $renderer = \Drupal::service('renderer');

  return $renderer->render($output);
}

/**
 * Preprocess function.
 */
function template_preprocess_liquid_slider(&$variables) {
  foreach ($variables['slides'] as $id => $slide) {
    $variables['slides'][$id]['attributes']->addClass(['slide', 'slide-' . $id]);
  }

  $variables['attributes'] = new Attribute([
    'class' => ['liquid-slider'],
    'id' => $variables['slider_id']
  ]);

  // Add Javascript settings and the library.
  $slider_options = json_decode('{' . $variables['settings']['json'] . '}');
  $variables['#attached']['drupalSettings']['liquidSliders'][$variables['slider_id']] = $slider_options;
  $variables['#attached']['library'][] = 'liquid/jquery.liquidSlider';
}

/**
 * @file
 * Defines the behaviors needed for Liquid Slider integration.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.liquid_slider = {
		attach: function (context) {

      if (!drupalSettings.hasOwnProperty('liquidSliders')) return;

      var sliders_data = drupalSettings.liquidSliders;

      var slider_defaults={
        autoHeight:true,
        autoSlide: true,
        autoSlideInterval: 6000,
        slideEaseFunction:'animate.css',
        slideEaseDuration:500,
        heightEaseDuration:500,
        animateIn:"fadeIn",
        animateOut:"fadeOut",
        pauseOnHover:false
      };
      
      for (var slider_id in sliders_data) {
        if (sliders_data.hasOwnProperty(slider_id)) {
          var selector = '#'+slider_id;
          $(context).find(selector).once('liquid_init').each(function () {
            var options = sliders_data[slider_id];

            // Merge defaults.
            for (var property in slider_defaults) {
              if (slider_defaults.hasOwnProperty(property)) {
                if (!options.hasOwnProperty(property)) {
                  options[property] = slider_defaults[property];
                }
              }
            }
            $(this).liquidSlider(options);
          });

        }
      }
      
    }
  }
}(jQuery, Drupal, drupalSettings));

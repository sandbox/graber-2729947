<?php

/**
 * @file
 * Definition of Drupal\liquid\Plugin\views\style\Liquid.
 */

namespace Drupal\liquid\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item as a liquid slider slide.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "liquid",
 *   title = @Translation("Liquid Slider"),
 *   help = @Translation("Display rows or entity in a Liquid Slider."),
 *   theme = "liquid_slider_view",
 *   display_types = {"normal"}
 * )
 */
class Liquid extends StylePluginBase {

  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * Denotes whether the plugin has an additional options form.
   *
   * @var bool
   */
  protected $usesOptions = TRUE;

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = FALSE;

  /**
   * Does the style plugin support grouping.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['json'] = array('default' => '');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['json'] = [
      '#type' => 'textarea',
      '#title' => t('Config JSON'),
      '#default_value' => $this->options['json']
    ];
  }

}

To use Liquid Slider as a view display handler, simply install this module and select "Liquid Slider" for your display plugin when editing a view.

Settings:

Settings should be placed in the settings textarea as a JSON object, without {}.
Example:

"autoSlide": true,
"autoSlideInterval": 6000
